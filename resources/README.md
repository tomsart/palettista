Notes on resources
==================


`sRGB-elle-V2-srgbtrc.icc` is licensed CC-by-SA, by Elle Stone, 2015. This file is found when
you export palettes from Krita, and is used here for creation of KPL palettes to be used in Krita.


