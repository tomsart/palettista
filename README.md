
Palettista
==========

This is a Godot based project that lets you sample colors from images,
and export to various palette and gradient formats.

Currently, you can export to Gimp gradient or palette, Krita gradient or palette,
Inkscape gradient or palette, Scribus palette, Swatchbooker palette,
CSS color classes, CSS gradients, or an SVG file with a grid of colors.

Currently in __Extreme Prototype__ stage of development.

Running
-------
I haven't done real builds for this yet, so to run, you
need the [Godot Engine](https://godotengine.org) installed, and open
this project within it. Then just run the project (by pressing F5)!

TODO
====
Features:
- gradients that take into account physical spacing
- sort palette by: bw(done), hue, color name
- better export dialog, then lets you select output type in the dialog
- export palette resource for Godot
- DONE sample from line for gradient
- DONE export Gimp palette (also usable in Inkscape)
- DONE export Gimp gradient (also usable in Inkscape)
- DONE export Krita palette
- DONE export Krita gradient (doesn't seem to have an explicit gradient format, but can import Gimp GGR or svg)
- DONE export Scribus colors
- DONE export css colors
- DONE export css gradient
- DONE export svg palette grid
- DONE (as of 4.2.2) check krita export unzip error when next godot out: https://github.com/godotengine/godot/pull/86985
- DONE export Swatchbooker

ux to fix:
- need undo/redo
- hide line when not used
- gradient sample needs to use distance 
- focus stays on number after pressing enter, should have feedback when number is set
- should only create new line when dragging past drag threshhold
- need better import dialog, default system dialog easily leaves out image preview!!
- bug: if you draw line fast, there's extra segments sticking out from some points
- i18n
- DONE try to equalize sampled points
- DONE dragging point off should turn red or show trash can to indicate it will be deleted
- DONE current point should display above everyone else
- DONE hide line behind point center while dragging so we can see all the way through hovered points
- DONE should have some "export done" toast -> using ToastParty addon from AssetLib
- DONE center image, scale to fit

Done:
- DONE make rim line less, and more less when hovered
- DONE draw line for palette points
- DONE random sample
- DONE init color name with something reasonable
- DONE point balls should be scale invariant
- DONE be able to delete points

Stretch goals
------------
- color sample from anywhere on screen, not just in window.. maybe import screenshot?
- assign color profile. perhaps derive from image?
- themeable interface
- import? Edit random palettes, not necessarily tied to specific images
  - change color name on click, hover name on mouse over
  - assign number of columns for palette?


Research
========
- Color management in Godot: https://github.com/godotengine/godot-proposals/issues/903
- Oklab review: https://raphlinus.github.io/color/2021/01/18/oklab-critique.html
- Tons of file format descriptions from Swatchbooker's creator: http://www.selapa.net/swatches/colors/fileformats.php
- Command line color converter: https://github.com/sharkdp/pastel
- Xkcd color survey: https://blog.xkcd.com/2010/05/03/color-survey-results/
