extends Node2D


var follow_node : Node2D


func _process(delta):
	if follow_node && follow_node.is_queued_for_deletion():
		follow_node = null
		return
	if follow_node && visible:
		global_position = follow_node.global_position + 25 * Vector2.DOWN


func Track(node):
	follow_node = node
	if !node:
		visible = false

func Open():
	$"Animated Lid/AnimationPlayer".play("Open")
	visible = true


func Close():
	$"Animated Lid/AnimationPlayer".play_backwards("Open")

