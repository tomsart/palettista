extends Node2D

## MUST BE > 1.0
@export var wheel_zoom_step := 1.1

@export var min_zoom := .1
@export var max_zoom := 50

signal new_canvas_scale


func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				ZoomIn(event.position)
			elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				ZoomOut(event.position)
			elif event.button_index == MOUSE_BUTTON_LEFT:
				#print ("mouse down at Canvas")
				pass
	elif event is InputEventMouseMotion:
		#print ("mm: ", event.position)
		
		if Input.is_mouse_button_pressed(MOUSE_BUTTON_MIDDLE):
			position += event.relative


func ZoomIn(pos: Vector2):
	var old_point := to_local(pos)
	var old_scale := scale
	
	if scale.length() * wheel_zoom_step > max_zoom:
		scale = max_zoom * Vector2.ONE
	else:
		scale *= wheel_zoom_step
	position += pos - to_global(old_point)
	 
	if old_scale != scale: new_canvas_scale.emit()


func ZoomOut(pos: Vector2):
	var old_point := to_local(pos)
	var old_scale := scale
	
	if scale.length() / wheel_zoom_step < min_zoom:
		scale = min_zoom * Vector2.ONE
	else:
		scale /= wheel_zoom_step
	position += pos - to_global(old_point)
	
	if old_scale != scale: new_canvas_scale.emit()



