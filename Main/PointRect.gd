extends ColorRect
class_name PointRect

#
# Class for ColorRect in the palette
#

var point : ColorPoint


func main():
	return get_node("../../..")


func _on_mouse_entered():
	main().PointRectEntered(self)


func _on_mouse_exited():
	main().PointRectExited(self)


var _hover := 0.0
func set_hover(value: float):
	_hover = value;
	#$Point.material.set_shader_parameter("hover", _hover)

func Hover():
	print ("hover ", name)
	#var tween = get_tree().create_tween()
	#tween.tween_method(set_hover, _hover, 1.0, .15)

func Unhover():
	print ("unhover ", name)
	#var tween = get_tree().create_tween()
	#tween.tween_method(set_hover, _hover, 0.0, .15)

