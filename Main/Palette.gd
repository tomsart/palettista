extends Control


@export var point_size := 20.0
@export var point_prototype: PackedScene
@export var adjustment_force := 1.0

@export var starting_image : String = ""

var num_columns_hint : int = 0
var palette_name := "Palette"

var hovered_point : PointInfo
var current_point : PointInfo

var gradient_param : GradientTexture1D
var gradient: Gradient

@onready var trash_can = $TrashCan


#--------------------- Undo/Redo -------------------------
class Undo:
	enum What { MovePoint, NewImage, AddPoint, DeletePoint, RenamePalette, RenameColor }
	var what := What.MovePoint
	var old_data
	var new_data

var undo_stack := []


#--------------------- PointInfo -------------------------

class PointInfo:
	var name: String
	var t: float # for gradient position
	var node: Node2D # draggable screen point
	var palette_rect: ColorRect
	var adjust: float = 0.0 # autocomputed for equalizing points
	var dist: float = 0.0 # dist along originating line
	var raw_p: float = 0.0
	func _init(_name, _t, _node, prect):
		self.name = _name
		self.t = _t
		self.node = _node
		self.palette_rect = prect

var points: Array[PointInfo] = []


enum FormatTypes {
		Default = 0,
		GimpGPL,      # Gimp palette
		GimpGGR,      # Gimp gradient
		KritaKPL,     # Krita palette file, a zip of a few files
		ScribusXML,   # a <SCRIBUSCOLORS> block
		CSSPalette,   # export a bunch of class defs with background set to color, or a css gradient
		CSSGradient,  # export either a linear-gradient or radial-gradient
		SVGGrid,      # export an svg file with an object using the colors
		Swatchbooker  # zip format with several files inside
	}
	

func _ready():
	for child in $BottomUI/PaletteColors.get_children():
		child.queue_free()
	$Canvas.new_canvas_scale.connect(_onCanvasScale)
	
	gradient_param = $BottomUI/Gradient.material.get_shader_parameter("gradient")
	gradient = gradient_param.gradient
	
	$Canvas/Line.visible = false
	
	if starting_image != "":
		push_error("IMPLEMENT ME!")
	
	call_deferred("CenterImage")
	
	InitExportMenu()
	
	trash_can.visible = false


func _onCanvasScale():
	if points.size() == 0: return
	
	var _scale = point_size / 50.0 # note point image is 50 px wide, point_size is desired pixel size
	var new_scale = 1.0 / $Canvas.scale.x * _scale
	
	var old_scale = points[0].node.scale.x
	var to_scale = old_scale / new_scale
	
	for point in points:
		point.node.scale = Vector2(new_scale, new_scale)
		
	for i in range(points.size()-1):
		#points[i].node.LineTo(points[i+1].node.global_position)
		points[i].node.ScaleLines(to_scale)


func UniqueChildName(node_parent: Node, base: String):
	var i := 1
	var color_name : String
	var not_found := true
	var pts := node_parent
	while not_found:
		color_name = base + str(i)
		var found := false
		for c in range(pts.get_child_count()):
			if pts.get_child(c).name == color_name:
				i += 1
				found = true
				break
		if !found:
			break
	return color_name


func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == MOUSE_BUTTON_LEFT:
				# We are either starting a line to later distribute points on
				# OR we are clicking down then up to create a new point
				if hovered_point == null:
					#current_point = AddPointAt($Canvas/Points.to_local(event.position))
					#hovered_point = current_point
					#hovered_point.Hover()
					StartLineRecord($Canvas/Points.to_local(event.position))
				else:
					current_point = hovered_point
		else:
			if event.button_index == MOUSE_BUTTON_LEFT:
				if recording_line:
					StopLineRecord()
					if raw_points.size() <= 2:
						hovered_point = AddPointAt($Canvas/Points.to_local(event.position))
						hovered_point.node.Hover()
						UpdateGradient()
					else:
						DistributeOnLine()
						UpdateGradient()
				if current_point:
					var xy = FindImageCoordNormalized(current_point.node)
					if xy.x < 0 || xy.x > 1 || xy.y < 0 || xy.y > 1:
						# delete point
						if hovered_point == current_point:
							hovered_point = null
						current_point.node.color_rect.queue_free()
						current_point.node.queue_free()
						DeletePoint(current_point)
						#current_point = null
				current_point = null
	
	elif event is InputEventMouseMotion:
		if current_point:
			var p1 : Vector2 = current_point.node.get_parent().to_local(Vector2.ZERO)
			var p2 : Vector2 = current_point.node.get_parent().to_local(event.relative)
		
			current_point.node.position += p2 - p1
			var cur_index = points.find(current_point)
			if cur_index >= 1: points[cur_index-1].node.LineTo(current_point.node.global_position)
			if cur_index < points.size()-1: current_point.node.LineTo(points[cur_index+1].node.global_position)
			
			if FindColor(current_point.node):
				if trash_can.visible:
					trash_can.Track(null)
			else:
				if !trash_can.visible:
					trash_can.Track(current_point.node)
					trash_can.Open()
			UpdateGradient()
			
		if recording_line:
			UpdateLine($Canvas/Points.to_local(event.position))
			#UpdateGradient()
	
	elif event is InputEventKey:
		if event.physical_keycode == KEY_SPACE:
			CenterImage()
			get_viewport().set_input_as_handled()


func AddPointAt(image_pos: Vector2) -> PointInfo:
	var new_point = point_prototype.instantiate()
	$Canvas/Points.add_child(new_point)
	new_point.scale = (1.0 / $Canvas.scale.x * (point_size / 50.0)) * Vector2.ONE
	new_point.position = image_pos
	new_point.name = "Point1"
	var col = Vector4(randf(), randf(), randf(), 1.)
	new_point.get_node("Point").material.set_shader_parameter("col", col)
	new_point.color_rect = PointRect.new()
	new_point.color_rect.color = Color(col.x, col.y, col.z, col.w)
	new_point.color_rect.point = new_point
	new_point.color_rect.name = UniqueChildName($BottomUI/PaletteColors, "Color")
	new_point.color_rect.custom_minimum_size = Vector2(20,20)
	new_point.color_rect.size_flags_horizontal = SIZE_EXPAND_FILL
	$BottomUI/PaletteColors.add_child(new_point.color_rect)
	FindColor(new_point)
	_onCanvasScale() # does them all instead of just 1, but hey it works
	points.append(PointInfo.new(new_point.color_rect.name, -1, new_point, new_point.color_rect))
	if points.size() > 1:
		points[points.size()-2].node.LineTo(new_point.global_position)
	return points[points.size()-1]


# remove point from points array
func DeletePoint(point_info: PointInfo):
	trash_can.Track(null)
	
	var index = points.find(point_info)
	if index < 0: return
	
	#var node : Node2D = point_info.node
	if current_point == point_info:
		current_point = null
	if hovered_point == point_info:
		hovered_point = null
	
	point_info.node.queue_free()
	point_info.palette_rect.queue_free()
	points.erase(point_info)
	
	if index > 0 && index < points.size()-1:
		points[index-1].node.LineTo(points[index].node.global_position)
	if index == points.size() && points.size() > 0:
		points[points.size()-1].node.ClearLine()
	return


#-------------------------------- Points --------------------------------

#func FindIndex(point_node) -> int:
#	for p in points.size():
#		if points[p].node == point_node:
#			return p
#	return -1

func FindPointInfo(point_node) -> PointInfo:
	for p in points.size():
		if points[p].node == point_node:
			return points[p]
	return null


func PointEntered(point):
	print("point enter: ", point.name)
	if !current_point:
		if hovered_point:
			hovered_point.node.Unhover()
		hovered_point = FindPointInfo(point)
		hovered_point.node.Hover()


func PointExited(point):
	print("point exit: ", point.name)
	if hovered_point != null && hovered_point.node == point && hovered_point != current_point:
		hovered_point.node.Unhover()
		hovered_point = null


func FindImageCoordNormalized(point) -> Vector2:
	var image_node = $Canvas/Image
	var image_point : Vector2 = image_node.to_local(point.to_global(Vector2.ZERO)) 
	var image : Image = image_node.texture.get_image()
	var image_size := image.get_size()
	var px = Vector2i(int(image_point.x + image_size.x/2.0), int(image_point.y + image_size.y/2.0))
	return Vector2(px.x / float(image_size.x), px.y / float(image_size.y))

## Return true for point on image, else false
func FindColor(point: ColorPoint) -> bool:
	var image_node = $Canvas/Image
	var image_point : Vector2 = image_node.to_local(point.to_global(Vector2.ZERO)) 
	var image : Image = image_node.texture.get_image()
	var image_size := image.get_size()
	var px = Vector2i(int(image_point.x + image_size.x/2.0), int(image_point.y + image_size.y/2.0))
	
	#print (image_point, "  size: ", image_size)
	if px.x >= 0 && px.x < image_size.x && px.y >= 0 && px.y < image_size.y:
		var col := image.get_pixelv(px)
		point.get_node("Point").material.set_shader_parameter("col", col)
		point.color_rect.color = col
		return true
	
	return false


#-------------------------- Load ----------------------

func _onLoadImagePressed():
	LoadImageDialog()


func LoadImageDialog():
	$ImageSelect/LoadImageDialog.popup()

func _on_file_dialog_confirmed():
	#print ("file dialog confirmed")
	pass


func _on_file_dialog_file_selected(path):
		# we are loading
	print ("load image: ", path)
	
	var image := Image.load_from_file(path)
	var texture := ImageTexture.create_from_image(image)
	$Canvas/Image.texture = texture
	
	CenterImage()
	
	$ImageSelect.visible = false


#------------------------------- Other Signals -------------------------

func _on_random_pressed():
	CreateRandom()


func _on_number_increase_pressed():
	var num := int($TopUI/Number.text)
	num += 1
	$TopUI/Number.text = str(num)


func _on_number_decrease_pressed():
	var num := int($TopUI/Number.text)
	if num > 1:
		num -= 1
		$TopUI/Number.text = str(num)


#--------------------------- General Actions --------------------------

func ClearPoints():
	if hovered_point:
		hovered_point.node.Unhover()
		hovered_point = null
	current_point = null
	
	points = []
	for child in $Canvas/Points.get_children():
		child.queue_free()
	for child in $BottomUI/PaletteColors.get_children():
		child.queue_free()


func CreateRandom():
	var number = int($TopUI/Number.text)
	if number <= 0: return
	
	ClearPoints()
	
	var image_size : Vector2 = $Canvas/Image.texture.get_image().get_size()
	
	for i in range(number):
		var pos := Vector2(randf() * image_size.x - image_size.x/2, randf() * image_size.y - image_size.y/2)
		AddPointAt($Canvas/Image.transform * pos)
	
	SortPalette()
	UpdateGradient()


func CenterImage():
	var image_size : Vector2 = $Canvas/Image.texture.get_image().get_size()
	
	var vp_size := get_viewport_rect().size
	var center : Vector2 = vp_size/2
	var old_point : Vector2 = $Canvas/Image.to_global(Vector2.ZERO)
	
	print ("center: image size: ", image_size, ", vp size: ", vp_size)
	
	#var old_scale : Vector2 = $Canvas.scale
	var box : Vector2 = vp_size * .8
	var hscale : float = box.x / image_size.x
	var vscale : float = box.y / image_size.y
	var new_scale = min(hscale, vscale)
	
	$Canvas.scale = Vector2(new_scale, new_scale)
	$Canvas.position += center - old_point


#------------------------- Line recording -------------------------------

class RawPoint:
	var p: Vector2
	var dist: float
	func _init(pp: Vector2, d: float):
		p = pp
		dist = d

var raw_points : Array[RawPoint]= [] # one per mouse move event
var recording_line := false


func StartLineRecord(pos: Vector2):
	recording_line = true
	$Canvas/Line.clear_points()
	$Canvas/Line.add_point(pos)
	raw_points = [RawPoint.new(pos, 0)]
	$Canvas/Line.visible = true
	print ("Start line")

func StopLineRecord():
	recording_line = false
	$Canvas/Line.visible = false
	print ("stop line")

## Append pos as a RawPoint to raw_points array.
func UpdateLine(pos: Vector2):
	if pos != $Canvas/Line.points[-1]:
		$Canvas/Line.add_point(pos)
		raw_points.append(RawPoint.new(pos, (pos - raw_points[-1].p).length() + raw_points[-1].dist))
		#print ("update line ", pos)


func DistributeOnLine():
	if raw_points.size() < 2: return
	
	var number := int($TopUI/Number.text)
	while points.size() < number:
		AddPointAt(Vector2.ZERO)
	
	number = points.size()
	
	var total_dist : float = raw_points[-1].dist
	var ds : float = total_dist / (number-1)
	
	var pts : Array = points
	pts[0].node.position = raw_points[0].p
	pts[0].dist = 0.0
	pts[-1].node.position = raw_points[-1].p
	pts[-1].raw_p = raw_points.size()-1
	pts[-1].dist = total_dist
	
	var s := ds
	var p : int = 0
	var last_p : int = 0
	for i in range(1, pts.size()-1):
		while p < raw_points.size()-1 && raw_points[p+1].dist < s:
			p += 1
		var x: float = (s - raw_points[p].dist) / (raw_points[p+1].dist - raw_points[p].dist)
		pts[i].raw_p = p + x
		pts[i].node.position = lerp(raw_points[p].p, raw_points[p+1].p, x)
		pts[i].dist = lerp(raw_points[p].dist, raw_points[p+1].dist, x)
		pts[i].adjust = 0.0
		last_p = p
		s += ds
	
	# Get initial color
	for point in pts:
		FindColor(point.node)
	
	var equalize_points := true
	
	if equalize_points:
		# Equalize points by iterating a few times pushing points around
		# the line according to color difference with adjacent points
		var num_iterations := 10
		var adjust_power := adjustment_force * ds # this can be changed during each iteration
		
		for i in num_iterations:
			# First determine how must we want to adjust
			print ("----iteration ",i, ", adjustment power: ", adjust_power)
			for ii in range(1, pts.size()-1):
				var cp : Color = pts[ii-1].palette_rect.color
				var cc : Color = pts[ii  ].palette_rect.color
				var cn : Color = pts[ii+1].palette_rect.color
				var c_diff_prev : float = \
					sqrt((cc.r-cp.r)*(cc.r-cp.r)+(cc.g-cp.g)*(cc.g-cp.g)+(cc.b-cp.b)*(cc.b-cp.b))
				var c_diff_next : float = \
					sqrt((cc.r-cn.r)*(cc.r-cn.r)+(cc.g-cn.g)*(cc.g-cn.g)+(cc.b-cn.b)*(cc.b-cn.b))
				#var v_prev: Vector2 = pts[ii-1].node.position - pts[ii].node.position
				#var v_next: Vector2 = pts[ii+1].node.position - pts[ii].node.position
				#var d_prev : float = v_prev.length()
				#var d_next : float = v_next.length()
				pts[ii].adjust = c_diff_next - c_diff_prev
			
			for ii in pts.size():
				print(ii," dist: ", pts[ii].dist,", adj: ", pts[ii].adjust)
			
			# Now apply the adjustment
			for ii in range(1, pts.size()-1):
				var amt : float = adjust_power * pts[ii].adjust
				if pts[ii].dist + amt <= pts[ii-1].dist: amt = 0.0
				elif pts[ii].dist + amt >= pts[ii+1].dist: amt = 0.0
				pts[ii].dist += amt
				print (ii, " adjust: ", amt)
				# locate point as dist along raw_points
				FindPointOnRawline(pts[ii])
			
			# Update color sampling
			for ii in range(1, pts.size()-1):
				FindColor(pts[ii].node)
			
			adjust_power *= .8
	
	
	# Now all points have a position, we need to break apart the original
	# line so each point maintains their own segments
	s = pts[1].dist
	p = 0
	last_p = 0
	var line_pts := [] # the raw line per segment between final points
	for i in range(1, pts.size()-1):
		while p < raw_points.size()-1 && raw_points[p+1].dist < s:
			line_pts.append(raw_points[p].p)
			p += 1
		var x: float = (s - raw_points[p].dist) / (raw_points[p+1].dist - raw_points[p].dist)
		line_pts.append(pts[i].node.position)
		pts[i-1].node.LinesTo(line_pts)
		line_pts = [ pts[i].node.position ]
		last_p = p
		s = pts[i+1].dist
	
	# do last one special
	for i in range(p, raw_points.size()):
		line_pts.append(raw_points[i].p)
	pts[-2].node.LinesTo(line_pts)


func FindPointOnRawline(point: PointInfo) -> void:
	var p := 0
	while p < raw_points.size()-2 && raw_points[p].dist < point.dist:
		p += 1
	# so dist is between p and p+1
	var d : float = absf(raw_points[p].dist - raw_points[p+1].dist)
	var x : float = (point.dist - raw_points[p].dist) / d
	point.node.position = lerp(raw_points[p].p, raw_points[p+1].p, x)
	

#------------------------------- Gradients ----------------------------------

# Get a child of $CanvasPoints, ignoring those queued for delete.
func GetIthPoint(i: int):
	if i >= 0 && i < points.size(): return points[i].node
	#for child in $Canvas/Points.get_children():
	#	if child.is_queued_for_deletion(): continue
	#	i -= 1
	#	if i < 0: return child
	return null

## Make the gradient stops coincide with the palette points
func UpdateGradient():
	var num_points := points.size()
	#for child in $Canvas/Points.get_children():
	#	if !child.is_queued_for_deletion(): num_points += 1
	
	if num_points == 0: 
		return
	
	if num_points == 1:
		# gradient needs 2 points, so just duplicate the single point
		while gradient.get_point_count() > 2:
			gradient.remove_point(gradient.get_point_count()-1)
		var point = GetIthPoint(0)
		gradient.set_color(0, point.color_rect.color)
		gradient.set_color(1, point.color_rect.color)
		gradient.set_offset(1,1)
		$BottomUI/Gradient.material.set_shader_parameter("gradient", gradient)
		return
	
	while gradient.get_point_count() > num_points:
		gradient.remove_point(gradient.get_point_count()-1)
	
	var n : float = float(num_points-1)
	for i in range(num_points):
		var point: ColorPoint = GetIthPoint(i)
		if i >= gradient.get_point_count():
			gradient.add_point(i/n, point.color_rect.color)
		else:
			gradient.set_color(i,point.color_rect.color)
			gradient.set_offset(i, i/n)
	
	$BottomUI/Gradient.material.set_shader_parameter("gradient", gradient_param)


#--------------------- Sorting --------------------------

# true is b > a
func _sort_by_value(a,b) -> bool:
	var av : float = 0.2989*a.palette_rect.color.r + 0.5870*a.palette_rect.color.g + 0.1140*a.palette_rect.color.b
	var bv : float = 0.2989*b.palette_rect.color.r + 0.5870*b.palette_rect.color.g + 0.1140*b.palette_rect.color.b
	if bv > av: return true
	return false

# true is b > a
func _sort_by_hue(a,b) -> bool:
	var av : float = a.palette_rect.color.h
	var bv : float = b.palette_rect.color.h
	if bv > av: return true
	return false

func SortPalette():
	points.sort_custom(_sort_by_value)
	for i in range(points.size()):
		$BottomUI/PaletteColors.move_child(points[i].palette_rect, i)
		$Canvas/Points.move_child(points[i].palette_rect.point, i)


#-------------------------- Export ----------------------


func InitExportMenu():
	var menu : PopupMenu = $ExportFormatMenu
	menu.add_item("Gimp Palette GPL",  FormatTypes.GimpGPL)
	menu.add_item("Gimp Gradient GGR", FormatTypes.GimpGGR)
	menu.add_item("Scribus Colors",    FormatTypes.ScribusXML)
	menu.add_item("Krita Palette KPL", FormatTypes.KritaKPL)
	menu.add_item("CSS Colors",        FormatTypes.CSSPalette)
	menu.add_item("CSS Gradient",      FormatTypes.CSSGradient)
	menu.add_item("SVG Grid",          FormatTypes.SVGGrid)
	menu.add_item("Swatchbooker SBZ", FormatTypes.Swatchbooker)
	menu.id_pressed.connect(_on_ExportFormatMenu)


func _on_export_pressed():
	$ExportFormatMenu.visible = true

var last_export_as : FormatTypes

func _on_ExportFormatMenu(id):
	print ("export with format: ", FormatTypes.keys()[id])
	last_export_as = id
	
	var dialog : FileDialog = $ImageSelect/ExportToDialog
	match last_export_as:
		FormatTypes.GimpGPL      : dialog.title = "Save as Gimp GPL"
		FormatTypes.GimpGGR      : dialog.title = "Save as Gimp GGR"
		FormatTypes.ScribusXML   : dialog.title = "Save as Scribus XML"
		FormatTypes.KritaKPL     : dialog.title = "Save as Krita KPL"
		FormatTypes.CSSPalette   : dialog.title = "Save as CSS Palette"
		FormatTypes.CSSGradient  : dialog.title = "Save as CSS Gradient"
		FormatTypes.SVGGrid      : dialog.title = "Save as SVG Grid"
		FormatTypes.Swatchbooker : dialog.title = "Save as Swatchbooker"
	
	$ImageSelect/ExportToDialog.popup()

func _on_export_to_selected(path):
	print ("export to: ", path)
	
	var status := true
	match last_export_as:
		FormatTypes.GimpGPL      : status = ExportGimpGPL(path)
		FormatTypes.GimpGGR      : status = ExportGimpGGR(path)
		FormatTypes.ScribusXML   : status = ExportScribusXML(path)
		FormatTypes.KritaKPL     : status = ExportKritaKPL(path)
		FormatTypes.CSSPalette   : status = ExportCSSPalette(path)
		FormatTypes.CSSGradient  : status = ExportCSSGradient(path)
		FormatTypes.SVGGrid      : status = ExportSVGGrid(path)
		FormatTypes.Swatchbooker : status = ExportSwatchbooker(path)
		_:
			push_error("Invalid export format! ", last_export_as)
			ToastParty.show({
				"text": "Unimplemented format!",
				"text_size": 25,                # Text (font) size
				"bgcolor": Color(.5, .2, .2, 0.7), # Background Color
				"color": Color(1, 1, 1, 1),     # Text Color
				"gravity": "top",               # top or bottom
				"direction": "center",           # left or center or right
				"use_font": false,               # Use custom ToastParty font
			})
			return
	
	if status:
		ToastParty.show({
			"text": "Exported!",
			"text_size": 25,                # Text (font) size
			"bgcolor": Color(.2, .5, .2, 0.7), # Background Color
			"color": Color(1, 1, 1, 1),     # Text Color
			"gravity": "top",               # top or bottom
			"direction": "center",           # left or center or right
			"use_font": false,               # Use custom ToastParty font
		})
	else:
		ToastParty.show({
			"text": "Error exporting, dangit!",
			"text_size": 25,                # Text (font) size
			"bgcolor": Color(.5, .2, .2, 0.7), # Background Color
			"color": Color(1, 1, 1, 1),     # Text Color
			"gravity": "top",               # top or bottom
			"direction": "center",           # left or center or right
			"use_font": false,               # Use custom ToastParty font
		})


#-------------------------------------------------------------------------------
#--------------------------------- Gimp GPL ------------------------------------
#-------------------------------------------------------------------------------

func ExportGimpGPL(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false
		
	file.store_line("GIMP Palette");
	file.store_line("Name: " + (palette_name if !palette_name.is_empty() else "Untitled"))
	if num_columns_hint > 0: file.store_line("Columns: %d" % num_columns_hint)
	file.store_line("#")

	for child in points:
		var color = child.palette_rect.color
		file.store_string("%3d %3d %3d " % [color.r8, color.g8, color.b8])
		
		if !child.name.is_empty():
			file.store_line(child.name)
		else:
			file.store_line("%02x%02x%02x" % [color.r8, color.g8, color.b8])
	
	return true


#-------------------------------------------------------------------------------
#--------------------------------- Gimp GGR ------------------------------------
#-------------------------------------------------------------------------------

func ExportGimpGGR(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false

	file.store_line("GIMP Gradient")
	file.store_line("Name: " + (palette_name if !palette_name.is_empty() else "Untitled"))
	file.store_line(str(points.size() - 1))

	 #  0          Left endpoint coordinate
	 #  1          Midpoint coordinate
	 #  2          Right endpoint coordinate
	 #  3          Left endpoint R
	 #  4          Left endpoint G
	 #  5          Left endpoint B
	 #  6          Left endpoint A
	 #  7          Right endpoint R
	 #  8          Right endpoint G
	 #  9          Right endpoint B
	 # 10          Right endpoint A
	 # 11          Blending function type
	 # 			0 = "linear"
	 # 			1 = "curved"
	 # 			2 = "sinusoidal"
	 # 			3 = "spherical (increasing)"
	 # 			4 = "spherical (decreasing)"
	 # 			5 = "step")
	 # 12          Coloring type
	 # 			0 = "RGB"
	 # 			1 = "HSV CCW"
	 # 			2 = "HSV CW"
	 # 13          Left endpoint color type
	 # 			0 = "fixed"
	 # 			1 = "foreground",
	 # 			2 = "foreground transparent"
	 # 			3 = "background",
	 # 			4 = "background transparent"
	 # 14          Right endpoint color type
	
	for c in range(points.size() - 1):
		# todo: need to handle non-rgba
		var child : ColorRect = points[c].palette_rect
		var nchild : ColorRect = points[c+1].palette_rect
		var color := child.color
		var ncolor := nchild.color
		var t := 0.0
		var nt := 1.0
		file.store_line("%.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %d %d %d %d" % [
				t, # left
				(t+nt)/2, # mid point
				nt, # right
				color.r8, # left r
				color.g8, # left g
				color.b8, # left b
				color.a8, # left a
				ncolor.r8, # right r
				ncolor.g8, # right g
				ncolor.b8, # right b
				ncolor.a8, # right a
				0, # blending function
				0, # Coloring type
				0, # left color type
				0  # right color type
				]
			)
	
	return true


#-------------------------------------------------------------------------------
#--------------------------------- Scribus XML ---------------------------------
#-------------------------------------------------------------------------------

func ExportScribusXML(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false
		
	file.store_line("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	file.store_line("<SCRIBUSCOLORS Name=\"%s\">" % [ (palette_name if !palette_name.is_empty() else "Untitled") ] )
	
	for child in points:
		var color : Color = child.palette_rect.color
		if child.name.is_empty():
			#TODO: need to account for non-rgb
			file.store_line("  <COLOR RGB=\"#%02X%02X%02X\" NAME=\"#%02X%02X%02X\" />" % [
				color.r8, color.g8, color.b8,
				color.r8, color.g8, color.b8
				] )

		else:
			file.store_line("  <COLOR RGB=\"#%02X%02X%02X\" NAME=\"%s\" />" % [
				color.r8, color.g8, color.b8, child.name ] )
	
	file.store_line("</SCRIBUSCOLORS>");
	
	return true;


#-------------------------------------------------------------------------------
#--------------------------------- Krita KPL -----------------------------------
#-------------------------------------------------------------------------------

func ExportKritaKPL(path) -> bool:
	var zip := ZIPPacker.new()
	var err := zip.open(path)
	if err != OK:
		push_error("Could not open file for writing: ", path)
		return false
	
	zip.start_file("mimetype")
	zip.write_file("application/x-krita-palette".to_utf8_buffer());
	zip.close_file()
	
	zip.start_file("profiles.xml")
	zip.write_file("<Profiles></Profiles>".to_utf8_buffer());
	zip.close_file()
	
	# Something along the lines of:
	#  <ColorSet rows="20" version="2.0" comment="" columns="16" name="TestPalette">
	#   <ColorSetEntry spot="false" bitdepth="U16" name="Color 1" id="1">
	#    <RGB space="sRGB-elle-V2-srgbtrc.icc" b="0.556862771511078" r="0.996078431606293" g="0.760784327983856"/>
	#    <Position column="0" row="0"/>
	#   </ColorSetEntry>
	#  </ColorSet>
	
	var buf : String = ""
	
	var num_cols : int = num_columns_hint if num_columns_hint > 0 else 16
	@warning_ignore("integer_division")
	var rows = points.size() / num_cols + 1

	buf += "<ColorSet rows=\"%d\" version=\"2.0\" comment=\"\" columns=\"%d\" name=\"%s\">\n" % \
					[ rows, num_cols, (palette_name if !palette_name.is_empty() else "Untitled") ]

	var row := 0
	var col := 0
	var space := "" #TODO!! replace with real colorspace

	#for point in points: #TODO finish replacing error prone node child stepping for colors
	for child in points:
		var color : Color = child.palette_rect.color
		
		if child.name.is_empty():
			#TODO: need to account for non-rgb
			buf += "  <ColorSetEntry spot=\"false\" bitdepth=\"U8\" name=\"#%02X%02X%02X\" >\n" % \
				[ color.r8,
				  color.g8,
				  color.b8
				]
		else:
			buf += "  <ColorSetEntry spot=\"false\" bitdepth=\"U8\" name=\"%s\" />\n" % \
				[ child.name ];
	
		buf += "    <RGB space=\"%s\" r=\"%f\" g=\"%f\" b=\"%f\" />\n" % \
				[ space,
				  color.r,
				  color.g,
				  color.b
				]
	
		buf += "    <Position column=\"%d\" row=\"%d\" />\n" % [col, row]
		buf += "  </ColorSetEntry>\n"
		col += 1
		if col % num_cols == 0:
			col = 0
			row += 1
	
	buf += "</ColorSet>"
	
	zip.start_file("colorset.xml")
	zip.write_file(buf.to_utf8_buffer());
	zip.close_file()
	
	zip.close();
	return true


#-------------------------------------------------------------------------------
#--------------------------------- CSS Palette ---------------------------------
#-------------------------------------------------------------------------------

# Export two CSS classes per color, one for "background-color" and one for "color".
func ExportCSSPalette(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false
	
	file.store_line("/* Name: %s */" % [ (palette_name if !palette_name.is_empty() else "Untitled") ] )
	
	var color_name: String;
	var color_str: String;
	var fg: String = ""
	var bg: String = ""
	
	for point in points:
		var child : ColorRect = point.palette_rect
		var color : Color = child.color
		
		if child.name.is_empty():
			color_name = "hex%02x%02x%02x%02x" % [ color.r8, color.g8, color.b8, color.a8 ]
		else: color_name = child.name;
		color_name.replace(" ", "");
		
		color_str = "rgba(%3d, %3d, %3d, %f)" % [ color.r8, color.g8, color.b8, color.a ]
		
		fg += (".%s { color: %s; }\n" % [ color_name, color_str ]);
		bg += (".%s-bg { background-color: %s; }\n" % [ color_name, color_str ]);
	
	file.store_string(fg)
	file.store_string(bg)
	
	return true


#-------------------------------------------------------------------------------
#--------------------------------- CSS Gradient --------------------------------
#-------------------------------------------------------------------------------

# Export a CSS gradient with fallback to first color, so something like:
# <code>
#    background: rgb(100,255,100);
#    background: linear-gradient(45deg, rgba(100,255,100,1) 0%, rgba(255,100,50,1) 100%);
# </code>
func ExportCSSGradient(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false
	
	file.store_line("/* Name: %s */" % [ (palette_name if !palette_name.is_empty() else "Untitled") ] )
	
	var color_str: String
	
	# Fallback solid color:
	var first = points[0].palette_rect.color
	color_str = "rgba(%d, %d, %d, %f)" % [ first.r8, first.g8, first.b8, first.a ]
	file.store_line("background: %s;" % [ color_str ]);
	
	var is_linear := true #TODO! add radial option
	var p1 := Vector2.ZERO
	var p2 := Vector2(1,0)
	if is_linear:
		var v := p2 - p1
		var angle : float = 180.0 / PI * atan2(v.y, v.x)
		file.store_string("background: linear-gradient(%.2fdeg, " % [ angle ])
	else:
		file.store_string("background: radial-gradient(circle, ")
	
	# Full gradient:
	var x := 0.0
	var dx : float = 1.0 / (points.size() - 1) #FIXME! Must use actual gradient position
	
	for index in range(points.size()):
		var child : ColorRect = points[index].palette_rect
		var color : Color = child.color
		color_str = "rgba(%d, %d, %d, %f)" % [ color.r8, color.g8, color.b8, color.a ]
		
		file.store_string("%s %.2f%% " % [ color_str, 100*x ])
		if index < points.size() - 1:
			file.store_string(", ")
		x += dx
	
	file.store_line(");")
	
	return true


#-------------------------------------------------------------------------------
#--------------------------------- SVG Grid ------------------------------------
#-------------------------------------------------------------------------------

# Export an svg grid filled with the palette.
func ExportSVGGrid(filename: String) -> bool:
	var file := FileAccess.open(filename, FileAccess.WRITE)
	if !file:
		push_error("Could not open file for writing: ", filename)
		return false

	file.store_line("<svg width=\"100\" height=\"100\">")
	
	if !palette_name.is_empty():
		file.store_line("<!-- %s -->" % [ palette_name ])
	
	var color_str: String
	var xn: int
	var yn: int
	if num_columns_hint > 0:
		xn = num_columns_hint
	else:
		var aspect := 1.0;
		xn = int(ceil(sqrt(points.size() / aspect)))
		if xn == 0: xn = 1
	
	@warning_ignore("integer_division")
	yn = (points.size()-1) / xn + 1
	if yn <= 0: yn = 1
	
	var x := 0.0
	var y := 0.0
	var w := 100.0/xn
	var h := 100.0/yn

	# draw grid of colors
	for c in range(points.size()):
		var child : ColorRect = points[c].palette_rect
		var color : Color = child.color
		
		color_str = "rgb(%d, %d, %d)" % [ color.r8, color.g8, color.b8 ]
		
		file.store_line("  <rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:%s; fill-opacity:%f; stroke:none;\" />" % \
			[ x,y,w,h, color_str, color.a ])
		
		x += w
		if (c+1)%xn == 0:
			y += h
			x = 0
	
	file.store_line("</svg>")
	
	return true


#-------------------------------------------------------------------------------
#--------------------------------- Swatchbooker SBZ ----------------------------
#-------------------------------------------------------------------------------

# Export a zip containing structure similar to:
#   swatchbooker.sbz
#   profiles/
#     SomeProfile.icm
#     SecondProfile.icm
func ExportSwatchbooker(path) -> bool:
	var zip := ZIPPacker.new()
	var err := zip.open(path)
	if err != OK:
		push_error("Could not open file for writing: ", path)
		return false
	
	var buf : String = ""
	
	var num_cols : int = num_columns_hint if num_columns_hint > 0 else 16
	@warning_ignore("integer_division")
	var rows = points.size() / num_cols + 1
	
	
	var time_str : String
	var datetime := Time.get_datetime_dict_from_system()
	time_str = Time.get_datetime_string_from_datetime_dict(datetime, false)
	
	buf += (
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
		+ "<SwatchBook version=\"0.7\"\n"
		+ "  xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
		+ "  xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
		+ "  xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
		+ "  <metadata>\n"
		+ "    <dc:format>application/swatchbook</dc:format>\n"
		+ "    <dc:type rdf:resource=\"http://purl.org/dc/dcmitype/Dataset\" />\n"
		+ "    <dc:date>%s</dc:date>\n" #// 2010-04-03T10:09:07.600000Z
		# + "    <dc:description>%s</dc:description>\n"
		+ "    <dc:title>%s</dc:title>\n"
		# + "    <dc:title xml:lang=\"fr\">Exemple d'échantillonnier</dc:title>\n"
		# + "    <dcterms:license rdf:resource=\"http://creativecommons.org/licenses/by/3.0/\" />\n"
		+ "  </metadata>\n"
		# + "A fancy palette" // description
		) % [ time_str, (palette_name if !palette_name.is_empty() else "Untitled") ]
	
	buf += "  <materials>\n"
	
	var row := 0
	var col := 0
	var space := "" #TODO!! replace with real colorspace

	for c in range(points.size()):
	#for child in points:
		var child := points[c]
		var color : Color = child.palette_rect.color
		
		buf += "    <color>\n"
		buf += "      <metadata>\n"
		
		if child.name.is_empty():
			#TODO: need to account for non-rgb
			buf += "        <dc:title>#%02X%02X%02X</dc:title>\n" % \
				[ color.r8,
				  color.g8,
				  color.b8
				]
		else:
			buf += "        <dc:title>%s</dc:title>\n" % [ child.name ]
		buf += "        <dc:identifier>%d</dc:identifier>\n" % [ c ]
		buf += "      </metadata>\n"
		buf += "      <values model=\"RGB\">%f %f %f</values>\n" % \
					#//space,
				[ color.r,
				  color.g,
				  color.b
				]
		buf += "    </color>\n"
	buf += "  </materials>\n"

	buf += ("  <book columns=\"%d\" rows=\"%d\">\n") % [ num_cols, rows ];
	
	for c in range(points.size()):
		var child := points[c]
		var color : Color = child.palette_rect.color
		buf += "    <swatch material=\"%d\" />\n" % [c]
	buf += "  </book>\n"
	
	buf += "</SwatchBook>"
	
	zip.start_file("colorset.xml")
	zip.write_file(buf.to_utf8_buffer());
	zip.close_file()
	
	zip.close();
	return true
