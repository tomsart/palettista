extends Area2D
class_name ColorPoint

#
# Draggable point over an image
#


var color_rect : ColorRect


func _on_mouse_entered():
	print ("enter")
	get_node("../../..").PointEntered(self)


func _on_mouse_exited():
	print ("exit")
	get_node("../../..").PointExited(self)


var _hover := 0.0
func set_hover(value: float):
	_hover = value;
	$Point.material.set_shader_parameter("hover", _hover)

func Hover():
	print ("hover ", name)
	z_index = 2
	var tween = get_tree().create_tween()
	tween.tween_method(set_hover, _hover, 1.0, .08)

func Unhover():
	print ("unhover ", name)
	z_index = 1
	var tween = get_tree().create_tween()
	tween.tween_method(set_hover, _hover, 0.0, .08)


func LineTo(point: Vector2):
	point = to_local(point)
	var dist := point.length()
	var v = point.normalized()
	var r := 25.0
	if dist > r:
		$Line2D.points = [ r/2 * v, point - r/2 * v ]
	else:
		$Line2D.clear_points()


func LinesTo(raw_points):
	var ar := []
	for i in range(raw_points.size()):
		ar.append(transform.affine_inverse() * raw_points[i])
	#print ("points on ",name,": ", ar)
	$Line2D.points = ar

func ClearLine():
	$Line2D.clear_points()

func ScaleLines(to_scale):
	#print ("old: ", $Line2D.points)
	for i in range($Line2D.get_point_count()):
		$Line2D.points[i] *= to_scale
	#print ("new: ", $Line2D.points)
